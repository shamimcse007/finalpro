<?php
include_once("../../../../vendor/autoload.php");
use App\Seip\Id158554\Mobile\Mobile;


if($_SERVER['REQUEST_METHOD']=='POST')
{
    if(!empty($_POST['mobile_model'])&& !empty($_POST['mobile_name']) && !empty($_POST['mobile_price']))
    {
        if(preg_match("/([a-zA-Z0-9])/", $_POST ['mobile_model'])){
            $_POST['mobile_model']=filter_var($_POST['mobile_model'],FILTER_SANITIZE_STRING);
            $mobile = new Mobile();
            $mobile -> setData($_POST) -> store();
        }else
            {
              $_SESSION['message']="invalid input";
              header ('location:create.php');

             }

    }else{
        $_SESSION['message']="Input can't be empty";
        header('location:create.php');
    }

}else{
    header('location:create.php');
}
