<?php
include_once ('../../../../vendor/autoload.php');
use App\Seip\Id158554\Mobile\Mobile;
$obj= new Mobile();
$allData =$obj->trashed();
?>
<?php
if(isset($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>final project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body style="">

<table class="table table-bordered" style="width: 600px;margin:0 auto; position:relative; ">
    <h3> <button style="margin: 0 auto"><a href="create.php">Add Mobile Data</a> </button></h3>
    <h3> <button style="margin: 0 auto"><a href="index.php">Home list</a> </button></h3>
    <thead>
    <tr>
        <th>ID</th>
        <th>Mobile_Model</th>
        <th>Mobile_Name</th>
        <th>mobile_price</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $serial="1";
    foreach($allData as $key => $value) {?>
        <tr>
            <td><?php echo $serial++?></td>
            <td><?php echo $value['mobile_model']?></td>
            <td><?php echo $value['mobile_name']?></td>
            <td><?php echo $value['mobile_price']?></td>
            <td>
                <a href="show.php ?id=<?php echo $value['unic_id']?>">Show</a> &nbsp;&nbsp;|
                <a href="Delete.php ?id=<?php echo $value['unic_id']?>">Delate</a>&nbsp;|
                <a href="restore.php ?id=<?php echo $value['unic_id']?>">Restore</a>
            </td>
        </tr>
    <?php  }?>
    </tbody>
</table>

</body>
</html>
